const fetch = require('node-fetch');
const fs = require('fs');

const URL = 'https://habr.akovalev.ru';
const KEY = 'andres_lifecheck_key';
const TG_CHAT_ID = 227204397;
const TG_API_URL = 'https://api.telegram.org/';

const [ , script, TG_TOKEN ] = process.argv;

check();

async function check() {
    log('started...');

    const isUp = getState();
    const response = await fetch(URL);
    const html = await response.text();

    if (html.includes(KEY)) {
        log('server is up...');

        if (!isUp) {
            if (await sendMessage('Server is ok now!')) {
                saveState(true);
            } else {
                log('failed to notify user!');
            }
        }

        return;
    }

    log('server is down!');

    if (!isUp) {
        return;
    }

    if (await sendMessage('Oh no, server is down!')) {
        saveState(false);
    } else {
        log('failed to notify user!');
    }
}

function log(message) {
    const { date, time } = now();
    const logFile = `${script}.${date}.log`;

    fs.appendFileSync(logFile, `[${time}] ${message}\n`);
}

function now() {
    const now = new Date();
    const year = now.getFullYear(),
        month = now.getMonth() + 1,
        date = now.getDate(),
        hour = now.getHours(),
        minute = now.getMinutes(),
        second = now.getSeconds(),
        ms = now.getMilliseconds();

    return {
        date: `${n(date, 2)}.${n(month, 2)}.${year}`,
        time: `${n(hour, 2)}:${n(minute, 2)}:${n(second, 2)}.${n(ms, 3)}`
    };
}

function n(value, length) {
    const add = Math.max(length - value.toString().length, 0);
    const prefix = Array(add).fill(0).join('');

    return `${prefix}${value}`;
}

function getState() {
    const stateFile = `${script}.state`;

    try {
        return Boolean(fs.readFileSync(stateFile, 'utf8'));
    } catch {
        return false;
    }
}

async function sendMessage(message) {
    const response = await fetch(`${TG_API_URL}bot${TG_TOKEN}/sendMessage?chat_id=${TG_CHAT_ID}&text=${message}`);
    const { ok } = await response.json();

    return ok;
}

function saveState(value) {
    const stateFile = `${script}.state`;
    const data = value ? 'true' : '';

    fs.writeFileSync(stateFile, data);
}